import React from 'react';
import { Route,Switch } from "react-router-dom";
import ABC from "../Components/Pages/ABC";
import HOME from '../Components/Home/Home';
import ALJEZZERA  from '../Components/Pages/AL JEZZERA'
import BBC from '../Components/Pages/BBC'
import CNN from '../Components/Pages/CNN'
import News from "../Components/News";

const Routes = ({props}) => {
    return ( 
        <>
        <div className="main-content">

       
        <Switch>
        <Route exact path="/" component={HOME} ></Route>
        <Route exact path="/ABC" component={ABC} ></Route>
        <Route exact path="/ALJ" component={ALJEZZERA } ></Route>
        <Route exact path="/BBC" component={BBC} ></Route>
        <Route exact path="/CNN" component={CNN} ></Route>
        <Route exact path="/newsID" component={News}></Route>
        </Switch>
        </div>
        </>
     );
}
 
export default Routes;