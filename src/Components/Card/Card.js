import React from 'react';
import './Card.css'
import Image from '../../assets/black9.jpg'


const Card = ({description, image,title,Date}) => {

    
    return ( 
        <>
        <div className="Card">
            <div className="content">
                <div className="card-img">
                <img src={ image} alt=""/>
                </div>
            <div className="title">
                <h3>{title}</h3>
            </div>
            <div className="description">
            <h5>{description}</h5>
            </div>
            <p className="date"><span>Published:</span> {Date}</p>
            </div>
        </div>
       
        </>
     );
}
 
export default Card;