import React,{useEffect,useState} from 'react';
import { Link } from "react-router-dom";
import './Navigation.css'


const Navigation = () => {
    // STATE
 const [toggle,setToggle] = useState(false)
 const[off,setOff] =useState(false)

 //FUNCTIONS
    const showNavbar=()=>{
        const menu = document.querySelector('.header__toggle'),
        nav = document.querySelector('#nav-bar'),
         bodypd = document.querySelector('#body-pd'),
       
         headerpd = document.querySelector('.header')
        menu.addEventListener('click',()=>{
            nav.classList.toggle('show')
            menu.classList.toggle('bx-x')
            bodypd.classList.toggle('body-pd')
            headerpd.classList.toggle('body-pd')
            setToggle(!toggle)
        })
    
    }

    // USEEFFECT
    useEffect(()=>{
        showNavbar()
},[toggle])

const linkColor = document.querySelectorAll('.nav__link')
const home = document.querySelector(".nav__logo")

function colorlink(){
    if(linkColor){
        linkColor.forEach(i=>i.classList.remove('active'))
        this.classList.add('active')
    }
}


linkColor.forEach(i=>i.addEventListener("click",colorlink));

const Offlink=()=>{
    setOff(true)
    if(off === true){
        linkColor.forEach(i=>i.classList.remove('active')) 
    }
}




    return ( 
        <>
        <header className="header" id="header">
            
            <div onClick={showNavbar  } className="header__toggle">
            <i className='bx bx-menu'>   Menu</i>
     
            </div>
          
        </header>

        <div className="sidenavBar" id="nav-bar">
            <nav className="nav">
                <div>
                    <Link to="/" className="nav__logo" onClick={Offlink}>
                    <i className='bx bxs-news nav__logo-icon'></i>
                        <span className="nav__logo-name">News App</span>
                    </Link>
                    <div className="nav__list">
                        <Link to="/ABC" className="nav__link ">
                        {/* <i className='bx bxs-home'></i> */}
                            <span className="nav__name source">
                             ABC
                            </span>
                           
                        </Link>

                        <Link to="/ALJ" className="nav__link">
                        {/* <i className='bx bxs-home'></i> */}
                            <span className="nav__name source">
                               AL JEZIRRA
                            </span>
                        </Link>

                        <Link to="/BBC" className="nav__link">
                        {/* <i className='bx bxs-home'></i> */}
                            <span className="nav__name source">
                               BBC
                            </span>
                        </Link>

                        <Link to="/CNN" className="nav__link">
                        {/* <i className='bx bxs-home'></i> */}
                            <span className="nav__name source">
                               CNN
                            </span>
                        </Link>
                    </div>
                </div>
            </nav>
        </div>
        </>
     );
}
 
export default Navigation;