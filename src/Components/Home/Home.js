import React, { Component } from 'react';
import Message from '../Message/Message';
import Business from './HomeComponents/Business';
import Entertainment from './HomeComponents/Entertainment';
import GeneralNews from './HomeComponents/GeneralNews';
import Health from './HomeComponents/Health';
import Sports from "./HomeComponents/Sports"
import Tech from "./HomeComponents/Tech"
class Home extends Component {
    state={
        overHeadata:[]  
    }

    /// FUNCTION FOR FETCHING DATA FROM API
    Data=async ()=>{
    //    fetch('https://newsapi.org/v2/everything?q=general&apiKey=3cc251f1961642789acda5b06bcaa3a9')
    //    .then(respone=>respone.json())
    //    .then(data=>this.setState({
    //     overHeadata:data.articles.slice(19)
    //    }))
    }
       
   ///MOUNTING OF GENERALNEWS COMPONENT
    componentDidMount(){
        this.Data()
    }
     
    
    render() { 
       
        const { overHeadata}= this.state
        console.log( overHeadata)

     
        return ( 
            <>
                <h1>Home</h1>
                {overHeadata.length === 0 && 
                <Message />
                
                }

                {
                    overHeadata.length > 0 && 
                    <>
                    <GeneralNews />
                  
                    <Tech />
                    
                    <Business />
                     
                    <Health />
                     
                    <Sports />
                     
                    <Entertainment />
                    </>
                }
               
               
            </>
         );
    }
}
 
export default Home;