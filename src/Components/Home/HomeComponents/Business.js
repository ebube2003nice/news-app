import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'


class Business extends Component {

    state={
        Businessdata:[],
 
       
    }
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=business&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           Businessdata:data.articles.slice(18)
       }))
    }
       
   
    componentDidMount(){
        this.Data()
    }
     
       
   
    render() { 
        const {Businessdata}= this.state
        
      console.log(Businessdata)
        return ( 
            <>
            <div className="head">
            <h2 >Business</h2>
            </div>
           
          
          
<div className="Card-Content">

{this.state.Businessdata.map((list,id) => (
       <Card 
       key={list.title}
       description={list.description}
       image={list.urlToImage}
       title={list.title}
       Date={list.publishedAt}
       />

      ))}

</div>
        
            </>
         );
    }
}
 
export default Business;