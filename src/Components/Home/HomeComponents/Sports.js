import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'
import fecth from "../../../Functions Modules/FetchModule"



class Sports extends Component {

    state={
        dataset:[],
 
       
    }
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=sports&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           dataset:data.articles.slice(18)
       }))
    }
       
   
    componentDidMount(){
        this.Data()
      
    }
     
       
   
    render() { 
        const {dataset}= this.state
        
      console.log(dataset)
        return ( 
            <>
            <div className="head">
            <h2 >Sports</h2>
            </div>
           
          
          
<div className="Card-Content">

{this.state.dataset.map((list,id) => (
       <Card 
       key={list.title}
       description={list.description}
       image={list.urlToImage}
       title={list.title}
       Date={list.publishedAt}
       />

      ))}

</div>
        
            </>
         );
    }
}
 
export default Sports;