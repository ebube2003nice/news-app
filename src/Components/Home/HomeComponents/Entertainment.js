import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'


class Entertainment extends Component {

    state={
        Entertainmentdata:[],
 
       
    }
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=entertainment&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           Entertainmentdata:data.articles.slice(0,2)
       }))
    }
       
   
    componentDidMount(){
        this.Data()
    }
     
       
   
    render() { 
        const {Entertainmentdata}= this.state
        
      console.log(Entertainmentdata)
        return ( 
            <>
            <div className="head">
            <h2 >Entertainment</h2>
            </div>
           
          
          
<div className="Card-Content">

{this.state.Entertainmentdata.map((list,id) => (
       <Card 
       key={list.title}
       description={list.description}
       image={list.urlToImage}
       title={list.title}
       Date={list.publishedAt}
       />

      ))}

</div>
        
            </>
         );
    }
}
 
export default Entertainment;