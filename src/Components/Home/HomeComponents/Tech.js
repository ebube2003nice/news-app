import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'


class Tech extends Component {

    state={
        techdata:[],
 
       
    }
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=tech&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           techdata:data.articles.slice(18)
       }))
    }
       
   
    componentDidMount(){
        this.Data()
    }
     
       
   
    render() { 
        const {techdata}= this.state
        
      console.log(techdata)
        return ( 
            <>
            <div className="head">
            <h2 >Tech</h2>
            </div>
           
          
          
<div className="Card-Content">

{this.state.techdata.map((list,id) => (
       <Card 
       key={list.title}
       description={list.description}
       image={list.urlToImage}
       title={list.title}
       Date={list.publishedAt}
       />

      ))}

</div>
        
            </>
         );
    }
}
 
export default Tech;