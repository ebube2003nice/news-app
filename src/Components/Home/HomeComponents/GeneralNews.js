import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'
import { Link } from "react-router-dom";

class GeneralNews extends Component {
///STATE
    state={
        GeneralNewsdata:[]  
    }

    /// FUNCTION FOR FETCHING DATA FROM API
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=general&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           GeneralNewsdata:data.articles.slice(16)
       }))
    }
       
   ///MOUNTING OF GENERALNEWS COMPONENT
    componentDidMount(){
        this.Data()
    }
     
       
   ///JSX TEMPLATE RENDERED HERE
    render() { 
      //STATE DESTRUCTED
        const {GeneralNewsdata}= this.state
         console.log(GeneralNewsdata)
        return ( 
            <>
            {GeneralNewsdata.length===0 &&
            null
            }



            {GeneralNewsdata.length>0 &&
              <>
     <div className="head">
          <h2 >World</h2>
    </div>
    
    <div className="Card-Content">
        {/* MAPPING OUT DATAFROM THE API */}
        {this.state.GeneralNewsdata.map((list,id) => (
           
            
            <Card 
                key={list.title}
                description={list.description}
                image={list.urlToImage}
                title={list.title}
                Date={list.publishedAt}
                
            />
         
        ))}
</div>
              </>   
         }

            </>
         );
    }
}
 
export default GeneralNews;