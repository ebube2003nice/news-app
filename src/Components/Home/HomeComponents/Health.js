import React, { Component } from 'react';
import Card from "../../Card/Card"
import './Home.css'


class Health extends Component {

    state={
        Healthdata:[],
 
       
    }
    Data=()=>{
       fetch('https://newsapi.org/v2/everything?q=health&apiKey=3cc251f1961642789acda5b06bcaa3a9')
       .then(respone=>respone.json())
       .then(data=>this.setState({
           Healthdata:data.articles.slice(18)
       }))
    }
       
   
    componentDidMount(){
        this.Data()
    }
     
       
   
    render() { 
        const {Healthdata}= this.state
        
      console.log(Healthdata)
        return ( 
            <>
            <div className="head">
            <h2 >Health</h2>
            </div>
           
          
          
<div className="Card-Content">

{this.state.Healthdata.map((list,id) => (
       <Card 
       key={list.title}
       description={list.description}
       image={list.urlToImage}
       title={list.title}
       Date={list.publishedAt}
       />

      ))}

</div>
        
            </>
         );
    }
}
 
export default Health;