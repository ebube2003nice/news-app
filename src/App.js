import './App.css';
import Navigation from './Components/Navigation/Navigation';
import Routes from "./Routes/Routes";

function App() {
  return (
    <div className="App">
    <div className="container">
      <Navigation />
        <Routes />
    </div>
    </div>
  );
}

export default App;
